
 Here is where we write what we have done and our documentation

Make sure to DELETE the devel and build folders before you build. -savvas

-"This MY branch. There are other's like it but this one is mine"


Challenge 2 DONE :)
NOTES
	* The robot_perception class has 2 members:
		resolution [meters/pixels]
		origin['x','y'] in [meters, meters]
	* The navigation class calculates a path (list of points in PIXELS [float,float])
	* There is a PoseStamped message ready to be published called ps
	* Rviz wants the points in METERS so I just calculated each point as: ps.pose.position = resolution*path + origin for each item of the calculated path list
	* navigation appends the ps instances in a list named ros_path and published to RViz. AND DONE :) 
	* Screen Shot is included in Photos


Challenge 3 (almost) DONE 
NOTES
	* In the navigation.py  script I implemented a simple controller whose parameters I estimated via experimentation
	* The angular velocity is proportional to the angular error (with a piece-wise and saturation trick for faster concergence)
		If the angle error is greater than some limit the angular is immediatly maxxed out. Otherwise it follows a simple P controller, with gain K = maxAngular/errorSat
	* The Linear Velocity is a a GAUSSIAN function of the angular error so that the max value is 0.3 and the 68% of the distro is between values of [-7,7] degrees. That way we avoid any robot-dashing away from the path, yet we achieve more 			smooth cruising, than some switch-like controller
WARNING
	* it is possible that the robot fails to converge to the subtarget. No proof of that yet, but so far experiments are good...


Challenge 6
NOTES for da report
	* I checked the information-gain methods: Turns out that calculationg the entropy and estimated entropy of a cell in OGM doesn' help much. Using the coverage in a binary manner (if c(i,j) < 50 etc) is much more efficient AND pretty much the same.
	* I checked the topological methods, with cost estimation and selection. I decided to traverse the uncovered nodes of the topological map, until there are none left. 
PROBLEM: The nodes are NOT given i a graph. So it's node is evaluated independently to the others
TODO: implement cost functions for rotation and other variables

