#!/usr/bin/env python

import math
import time
import rospy
import numpy as np

from sensor_msgs.msg import Range
from sensor_msgs.msg import LaserScan
from geometry_msgs.msg import Twist

from sonar_data_aggregator import SonarDataAggregator
from laser_data_aggregator import LaserDataAggregator
from navigation import Navigation



def b1_func(x):
    if(x < -0.3 or x > 0.3):
        return math.copysign(0.3, x)
    else:
        return x

def b2_func(x):
    if(x < 0):
        return 0
    elif (x <= 0.3):
        return x
    else:
        return 0.3


# Class for assigning the robot speeds


class RobotController:

    # Constructor
    def __init__(self):

        # Debugging purposes
        self.print_velocities = rospy.get_param('print_velocities')

        # Where and when should you use this?
        self.stop_robot = False

        # Create the needed objects
        self.sonar_aggregation = SonarDataAggregator()
        self.laser_aggregation = LaserDataAggregator()
        self.navigation = Navigation()

        self.linear_velocity = 0
        self.angular_velocity = 0

        # Check if the robot moves with target or just wanders
        self.move_with_target = rospy.get_param("calculate_target")

        # The timer produces events for sending the speeds every 110 ms
        rospy.Timer(rospy.Duration(0.11), self.publishSpeeds)
        self.velocity_publisher = rospy.Publisher(
            rospy.get_param('speeds_pub_topic'), Twist,
            queue_size=10)
        # a variable for determining if obstacles are present in lIDAR
        # measurements
        self.no_obstacle = True
        # steps of velocity change, to avoid robot motor damage
        self.max_linear_evasion = 0.2
        self.max_angular_evasion = 0.2
    # This function publishes the speeds and moves the robot

    def publishSpeeds(self, event):

        # Produce speeds
        self.produceSpeeds()

        # Create the commands message
        twist = Twist()
        twist.linear.x = self.linear_velocity
        twist.linear.y = 0
        twist.linear.z = 0
        twist.angular.x = 0
        twist.angular.y = 0
        twist.angular.z = self.angular_velocity

        # Send the command
        self.velocity_publisher.publish(twist)

        # Print the speeds for debuggind purposes
        if self.print_velocities == True:
            print "[L,R] = [" + str(twist.linear.x) + " , " + \
                str(twist.angular.z) + "]"

    # Produces speeds from the laser
    def produceSpeedsLaser(self):
        scan = self.laser_aggregation.laser_scan
        linear = 0
        angular = 0

        ############################### NOTE QUESTION #########################
        # Check what laser_scan contains and create linear and angular speeds
        # for obstacle avoidance
        angle_increment = 0.006280044559389353
        steps_to_inc = (2.0943949222564697 - 1.74533) / angle_increment
        steps_to_inc = int(steps_to_inc)

        scan = scan[steps_to_inc:(len(scan) - steps_to_inc)]
        angle_start = -1.74533
        angle_end = 1.74533

        epsilon = 10 ** (-3)
        # calculate threshold for cutting distances
        sumF = np.array([0, 0])
        phi = angle_start
        num_rays = len(scan)
        # min_scan = min(scan);
        # obst = 0
        for obst_radius in scan:
            phi += angle_increment
            if (self.no_obstacle == True and np.abs(phi) > 0.3):
                f = np.array([0, 0])
            # if(self.no_obstacle == False and np.abs(phi) < 0.2 and obst_radius < 0.6):
            #     self.no_obstacle == True
            if(obst_radius > (0.6 * (self.linear_velocity)/0.3) or np.abs(phi) > 1.50): #1.57):
                f = np.array([0, 0])
            else:
                # f = np.array( [math.cos(phi),math.sin(phi) ]) / ( 1 + (math.pow(obst_radius,2) - 0.1)/epsilon )
                f = np.array([math.cos(phi), math.sin(phi)]) * np.exp(-(math.pow(obst_radius - 0.1, 2) / (2 * (0.3))))
            sumF = np.add(sumF, f)
        
        if(np.linalg.norm(sumF) == 0):
            self.no_obstacle = True
        else:
            self.no_obstacle = False
    #  print("This is scan: " + repr(len(scan)) + "\n")
        sumF = -(sumF)
        linear = math.tanh(sumF[0]) * self.max_linear_evasion
        delta_phi = math.atan2(sumF[1], sumF[0])
        # print(sumF)
        # print("linear :" + repr(linear) + "\n")
        # print("delta_phi :" + repr(delta_phi *180/np.pi) + "\n")
        angular = math.tanh(np.linalg.norm(sumF) * delta_phi) * self.max_angular_evasion

        return [linear, angular]

    # Combines the speeds into one output using a motor schema approach
    def produceSpeeds(self):

        # Produce target if not existent
        if self.move_with_target == True and \
                self.navigation.target_exists == False:

            # Create the commands message
            twist = Twist()
            twist.linear.x = 0
            twist.linear.y = 0
            twist.linear.z = 0
            twist.angular.x = 0
            twist.angular.y = 0
            twist.angular.z = 0

            # Send the command
            self.velocity_publisher.publish(twist)
            self.navigation.selectTarget()

        # Get the submodule's speeds
        [l_laser, a_laser] = self.produceSpeedsLaser()

        # You must fill these
        self.linear_velocity = 0
        self.angular_velocity = 0
        lambda1 = 0*0.65
        
        if self.move_with_target == True:
            [l_goal, a_goal] = self.navigation.velocitiesToNextSubtarget()
            ############################### NOTE QUESTION #####################
            # You must combine the two sets of speeds. You can use motor schema,
            # subsumption of whatever suits your better.
            if(self.no_obstacle == False):
                
                # Perfrom obstacle avoidance algorithm
                self.angular_velocity = b1_func(lambda1*a_laser + (1 - lambda1)* self.angular_velocity)
                self.linear_velocity = b2_func(lambda1*l_laser + (1 - lambda1)*self.linear_velocity)
            else:
                self.angular_velocity = b1_func((1- lambda1)*a_goal + (1-lambda1)*self.angular_velocity)
                self.linear_velocity = b2_func((1-lambda1)*l_goal + lambda1*self.linear_velocity)
            ###################################################################
        else:
            ############################### NOTE QUESTION #####################
            # Implement obstacle avoidance here using the laser speeds.
            # Hint: Subtract them from something constant
            l_goal = 0.3
            a_goal = 0.0
            if(self.no_obstacle == False):
                # Perfrom obstacle avoidance algorithm
                self.angular_velocity = b1_func(a_laser + self.angular_velocity)
                self.linear_velocity = b2_func(l_laser + self.linear_velocity)
            else:
                self.angular_velocity = b1_func(a_goal + self.angular_velocity)
                self.linear_velocity = b2_func(l_goal + self.linear_velocity)
            print(self.no_obstacle)
            print(self.linear_velocity)
            print(self.angular_velocity)
                
            ###################################################################
    # Assistive functions

    def stopRobot(self):
        self.stop_robot = True

    def resumeRobot(self):
        self.stop_robot = False
