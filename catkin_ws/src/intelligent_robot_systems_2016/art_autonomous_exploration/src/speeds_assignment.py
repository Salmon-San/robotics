#!/usr/bin/env python

import math
import time
import rospy
import numpy as np

from sensor_msgs.msg import Range
from sensor_msgs.msg import LaserScan
from geometry_msgs.msg import Twist

from sonar_data_aggregator import SonarDataAggregator
from laser_data_aggregator import LaserDataAggregator
from navigation import Navigation



def b1_func(x):
    if(x < -0.3 or x > 0.3):
        return math.copysign(0.3, x)
    else:
        return x

def b2_func(x):
    if(x < 0):
        return 0
    elif (x <= 0.3):
        return x
    else:
        return 0.3


# Class for assigning the robot speeds


class RobotController:

    # Constructor
    def __init__(self):

        # Debugging purposes
        self.print_velocities = rospy.get_param('print_velocities')

        # Where and when should you use this?
        self.stop_robot = False

        # Create the needed objects
        self.sonar_aggregation = SonarDataAggregator()
        self.laser_aggregation = LaserDataAggregator()
        self.navigation = Navigation()

        self.linear_velocity = 0
        self.angular_velocity = 0

        # Check if the robot moves with target or just wanders
        self.move_with_target = rospy.get_param("calculate_target")

        # The timer produces events for sending the speeds every 110 ms
        rospy.Timer(rospy.Duration(0.11), self.publishSpeeds)
        self.velocity_publisher = rospy.Publisher(
            rospy.get_param('speeds_pub_topic'), Twist,
            queue_size=10)
        
        # Bias of the laser-produced speeds
        if self.move_with_target:
            self.bias = 0.0;
            self.n=2
        else:
            self.bias = 0.50;
            self.n=2
                
        
    # This function publishes the speeds and moves the robot

    def publishSpeeds(self, event):

        # Produce speeds
        self.produceSpeeds()

        # Create the commands message
        twist = Twist()
        twist.linear.x = self.linear_velocity
        twist.linear.y = 0
        twist.linear.z = 0
        twist.angular.x = 0
        twist.angular.y = 0
        twist.angular.z = self.angular_velocity

        # Send the command
        self.velocity_publisher.publish(twist)

        # Print the speeds for debuggind purposes
        if self.print_velocities == True:
            print "[L,R] = [" + str(twist.linear.x) + " , " + \
                str(twist.angular.z) + "]"

    # Produces speeds from the laser
    def produceSpeedsLaser(self):
        scan = self.laser_aggregation.laser_scan
        ############################### NOTE QUESTION #########################
        # Check what laser_scan contains and create linear and angular speeds
        # for obstacle avoidance
        angle_increment = 0.006280044559389353
        steps_to_inc = (2.0943949222564697 - 1.74533) / angle_increment
        steps_to_inc = int(steps_to_inc)

        scan = scan[steps_to_inc:(len(scan) - steps_to_inc)]
        angle_start = -1.74533
        #angle_end = 1.74533

        # calculate threshold for cutting distances
        phi = angle_start
        u_obst = 0
        w_obst = 0
        for obst_radius in scan:
            phi += angle_increment
            u_obst += -np.cos(phi)/np.power(obst_radius + self.bias, self.n)#
            w_obst += -np.sin(phi)/np.power(obst_radius + self.bias, self.n)#
            
        return [u_obst, w_obst]

    # Combines the speeds into one output using a motor schema approach
    def produceSpeeds(self):

        # Produce target if not existent
        if self.move_with_target == True and \
                self.navigation.target_exists == False:

            # Create the commands message
            twist = Twist()
            twist.linear.x = 0
            twist.linear.y = 0
            twist.linear.z = 0
            twist.angular.x = 0
            twist.angular.y = 0
            twist.angular.z = 0

            # Send the command
            self.velocity_publisher.publish(twist)
            self.navigation.selectTarget()

        # Get the submodule's speeds
        [l_laser, a_laser] = self.produceSpeedsLaser()

        # You must fill these
        self.linear_velocity = 0
        self.angular_velocity = 0
        
        if self.move_with_target == True:
            [l_goal, a_goal] = self.navigation.velocitiesToNextSubtarget()
            self.angular_velocity = b1_func(a_goal+  0.00005*a_laser )
            self.linear_velocity = b2_func(l_goal +  0.0001*l_laser )
        else:
            l_goal = 0.2
            a_goal = 0.0
            self.angular_velocity = b1_func(a_goal + 0.1*a_laser)
            self.linear_velocity = b2_func(l_goal + 0.0003*l_laser)
                
        if self.print_velocities:
            print("Vel = " + str(self.linear_velocity) + " , " + str(self.angular_velocity))
            print("laser [v,a] = " + str(l_laser) + " , " + str(a_laser))
         


###################################################################
    # Assistive functions

    def stopRobot(self):
        self.stop_robot = True

    def resumeRobot(self):
        self.stop_robot = False
