#!/usr/bin/env python

import rospy
import random
import math
import time
import numpy as np
from timeit import default_timer as timer
from utilities import RvizHandler
from utilities import OgmOperations
from utilities import Print
from brushfires import Brushfires
from topology import Topology
import scipy


from path_planning import PathPlanning



# Class for selecting the next best target
class TargetSelection:
    
    # Constructor
    def __init__(self, selection_method):
        self.goals_position = []
        self.goals_value = []
        self.omega = 0.0
        self.radius = 0
        self.method = selection_method

        self.brush = Brushfires()
        self.topo = Topology()
        self.path_planning = PathPlanning()

        self.prev_target = [0, 0]

    def selectTarget(self, init_ogm, coverage, robot_pose, origin, \
        resolution, force_random = False):
        
        target = [-1, -1]

        if self.method == 'random' or force_random == True:
            # Find only the useful boundaries of OGM. Only there calculations
            # have meaning
            ogm_limits = OgmOperations.findUsefulBoundaries(init_ogm, origin, resolution)
    
            # Blur the OGM to erase discontinuities due to laser rays
            ogm = OgmOperations.blurUnoccupiedOgm(init_ogm, ogm_limits)
    
            # Calculate Brushfire field
            tinit = time.time()
            brush = self.brush.obstaclesBrushfireCffi(ogm, ogm_limits)
            
            # Random point
            target = self.selectRandomTarget(ogm, coverage, brush, ogm_limits)

        else: # the Frontier Based method is selected
        
            # Find only the useful boundaries of OGM. Only there calculations
            # have meaning
            ogm_limits = OgmOperations.findUsefulBoundaries(init_ogm, origin, resolution)
    
            # Blur the OGM to erase discontinuities due to laser rays
            ogm = OgmOperations.blurUnoccupiedOgm(init_ogm, ogm_limits)
    
            # Calculate Brushfire field
            tinit = time.time()
            brush = self.brush.obstaclesBrushfireCffi(ogm, ogm_limits)
            Print.art_print("Brush time: " + str(time.time() - tinit), Print.ORANGE)  
            
            tinit = time.time()
            # apply lapplace filter
            coverage_borders = scipy.ndimage.filters.laplace(coverage) 
            # separate coverage_borders where obstacles interfere
            boundaries_matrix = np.logical_and(coverage_borders > np.zeros(coverage_borders.shape), (ogm != (-1) * np.ones(ogm.shape)))
            # label the different frontiers
            s = np.ones((3,3), dtype = int)
            boundaries_labels, num_boundaries = scipy.ndimage.label(boundaries_matrix, structure = s)
            lbls = np.arange(1, num_boundaries+2)
    
            # find frontiers sizes and centers
            boundaries_sizes = np.array(scipy.ndimage.measurements.labeled_comprehension(boundaries_matrix, boundaries_labels, lbls, define_boundary_size, float, 0, True) )
            boundaries_centers = np.array(scipy.ndimage.measurements.labeled_comprehension(brush, boundaries_labels, lbls, define_boundary_center, float, 0, True) )
            Print.art_print("boundaries segmentation time: " + str(time.time() - tinit), Print.ORANGE)              
            
            # TARGET SELETION       
            # get the frontier with the largest size
            tinit = time.time()
            target = boundaries_centers[ np.argmax(boundaries_sizes) ]
            
            # Use Brushfire to get an unoccupied point and make it NEXT TARGET
            target = [int(target / ogm.shape[1]) , int(target % ogm.shape[1])]
            if(ogm[target[0], target[1]] < 0):
                # find the closest point with no obstacle using bfs
                point_list = [[target[0], target[1] + 1]]
                visited  = []
                while(ogm[point_list[-1]] < 0):
                    p = point_list[-1]
    
                    if([p[0] + 1, p[1]] not in point_list):
                        if(ogm[p[0] + 1, p[1]] < 0):
                            point_list.append([p[0] + 1, p[1]])
                        else:
                            target = [p[0] + 1, p[1]]
                            break
                    if([p[0] - 1, p[1]] not in point_list):
                        if(ogm[p[0] - 1, p[1]] < 0):
                            point_list.append([p[0] - 1, p[1]])
                        else:
                            target = [p[0] + 1, p[1]]
                            break
                    if([p[0], p[1] + 1] not in point_list):
                        if(ogm[p[0], p[1] + 1] < 0):
                            point_list.append([p[0], p[1] + 1])
                        else:
                            target = [p[0], p[1] + 1]
                            break
                    if([p[0], p[1] - 1] not in point_list):
                        if(ogm[p[0], p[1] - 1] < 0):
                            point_list.append([p[0], p[1] - 1])
                        else:
                            target = [p[0], p[1] - 1]
                            break
            print( "target = " + str(target) )
            Print.art_print("Target calculation-via-brush-field Time: " + str(time.time() - tinit), Print.ORANGE)  
            
            if(target == self.prev_target):
                force_random = True
       #endif
            
        self.prev_target = target
        return target

    
    
    def selectRandomTarget(self, ogm, coverage, brushogm, ogm_limits):
      # The next target in pixels
        # f = open('/home/sniper/projects/school/Robotics/ogm.txt', 'w')
        # f.truncate()
        # for i in brushogm:
        #    f.write(str(i))
        # f.close()

        tinit = time.time()
        next_target = [0, 0] 
        found = False
        while not found:
          x_rand = random.randint(0, ogm.shape[0] - 1)
          y_rand = random.randint(0, ogm.shape[1] - 1)
          if ogm[x_rand][y_rand] < 50 and coverage[x_rand][y_rand] < 50 and \
              brushogm[x_rand][y_rand] > 5:
            next_target = [x_rand, y_rand]
            found = True
        Print.art_print("Select random target time: " + str(time.time() - tinit), \
            Print.ORANGE)
        return next_target

def define_boundary_size(val, pos):
    # boundary_center = pos[len(pos)/2]
    # boundary_size = 
    return len(pos)

def define_boundary_center(val, pos):
    return pos[np.argmax(val)]


        
        
                
